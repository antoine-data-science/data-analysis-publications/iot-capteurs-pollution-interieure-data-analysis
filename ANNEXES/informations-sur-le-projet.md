<a name="haut-de-document"></a>Haut de document

Raccourcis directs:

* [Sommaire](#sommaire)
* [Contact](#contact)

---

**PROJET D'ANALYSE DE DONNÉES PROVENANT DE CAPTEURS DE POLLUTION DE L'AIR À L'INTÉRIEUR D'UN PETIT LABORATOIRE DE PRODUCTION (DE TYPE FABLAB)**

**DESCRIPTIF DU PROJET**

[Antoine Chevrier](https://www.linkedin.com/in/a5frddresszd)

Juillet 2022 - Juillet 2023

Ce document donne des informations descriptives sur ce projet

---

<a nmae="sommaire"></a>
# Sommaire

- [Résumé](#resume)
- [Approche méthodologique](#approche-methodologique)
- [Ouverture du projet](#ouverture)

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="resume"></a>
# A. RÉSUMÉ

Un dispositif de capteurs de qualité de l'air est positionné dans un fablab associatif. Un tableau de bord permet de visualiser les valeurs en temps réel. Mais les données ne sont pas analysées. Le projet consiste à analyser les données fournies sur une période de temps donné (2019 à 2022).

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="approche-methodologique"></a>
# B. APPROCHE MÉTHODOLOGIQUE RETENUE

<a name="iteration"></a>
**Itération de cycles complets:** aussi bien la méthode projet retenue que les étapes de réalisations des opérations de data-science, sont itérées par cycle complet? cela signifie que pour chaque itération, une boucle complète de toutes les étapes est réalisée.

<a name="iteration-progressive"></a>
**Itération progressive:** une série de 6 itérations progressives.
- Itération 1: à vide
- Itération 2: sans logique, avec des informations sans queue ni tête, juste pour voir la théorie
- Itération 3: avec les informations réelles les plus simples possibles, pour se rendre compte de l'écart entre la théorie et l'effet de la réalité des informations du projet
- Itération 4: avec les informations réelles, complètes
- Itération 5: vérifications, passage en revue
- Itération 6: finalisation complète

<a name="methode-projet"></a>
**Méthode projet universelle:** le projet est déroulé avec en toile de fond, la référence à l'une des  méthodes projets "universelles", de telle façon à savoir où on en est et à se repérer dans la progression, de l'ouverture du projet, à sa clôture.

**Les grandes phases du projet:**
- [I. OUVERTURE DU PROJET](#ouverture)
- [II. RECOLTE INFORMATIONS PRE-EXISTANTES](#recolte-pre-existantes)
- [III. ANALYSE DES INFORMATIONS PRE-EXISTANTES RÉCOLTÉES](#analyse-recolte)
- [IV. DECISION GO / NOGO](#decision)
- [V. PLANIFICATION](#planification)
- [VI. REALISATIONS](#realisations)
- [VII. EVALUATION DU PROJET](#evaluation)
- [VIII. TRANSMISSION DU PROJET](#transmission)
- [IX. FERMETURE, CLOTURE DU PROJET](#cloture)

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="ouverture"></a>
# I. OUVERTURE DU PROJET

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="recolte-pre-existantes"></a>
# II. RECOLTE INFORMATIONS PRE-EXISTANTES

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="analyse-recolte"></a>
# III. ANALYSE DES INFORMATIONS PRE-EXISTANTES RÉCOLTÉES

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="decision"></a>
# IV. DECISION GO / NOGO

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="planification"></a>
# V. PLANIFICATION

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="realisations"></a>
# VI. REALISATIONS

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="evaluation"></a>
# VII. EVALUATION DU PROJET

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="transmission"></a>
# VIII. TRANSMISSION DU PROJET

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="cloture"></a>
# IX. FERMETURE, CLOTURE DU PROJET

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="projets-connexes"></a>
# C. PROJETS CONNEXES

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="voir-aussi"></a>
# D. AUTRES INFORMATIONS

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)

<a name="sources"></a>
# E. SOURCES

Retour vers: [Haut de document](#haut-de-document) ou [Sommaire](#sommaire)