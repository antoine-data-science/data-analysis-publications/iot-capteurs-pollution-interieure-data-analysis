# iot-capteurs-pollution-interieure-data-analysis


## Name
Analyse Data-Science IoT de capteurs de pollution intérieur

## Description
Un dépot pour publier une analyse de données de capteurs de pollution intérieurs faite avec des notebooks jupyter en python

## Badges
Aucun

## Visuals
* [ ] à faire 

## Installation
* Pour lire les documents, aucune installation requise (lecture en ligne)
* Pour exécuter les notebooks, utilisez un service de notebook jupyter en ligne (Colab, Anaconda-Cloud, etc ... ou tout autre service disponible qui va bien) ou installez Jupyter sur votre machine (par exemple via une installation Anaconda)

## Usage
* Lorsque vous recherchez de l'inspiration pour analyser des data en provenance de capteurs IoT
* Lorsque vous recherchez de l'information sur ce que je fais en analyse de données

## Support
[Adressez moi un message](https://www.linkedin.com/in/a5frddresszd)

## Roadmap
* [X] creation du dépôt [==========>][100%] fait
* [ ] structuration préalable du dépôt [>---------] [10 %] en cours
* [ ] publication analyse preliminaire [-----------] [0 %] pas démarrée
* [ ] publication analyses approfondies [-----------] [0 %] pas démarrée

## Contributing
Faites comme vous le voulez

## Authors and acknowledgment
Auteur : [Antoine Chevrier](https://www.linkedin.com/in/a5frddresszd)


## License
Pas de licence, execepté pour les éléments qui en indiquent une

## Project status
Actif
